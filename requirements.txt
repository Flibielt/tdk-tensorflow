argparse~=1.4.0
numpy~=1.19.2  # To ensure compatibility with OpenCV on Raspberry Pi.
opencv-python~=4.5.3.56
tflite-runtime
tflite-support
absl-py~=0.10

matplotlib~=3.5.1

scikit-video~=1.1.11
