# Copyright 2021 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Utility functions to display the pose detection results."""

from typing import List

import cv2
import numpy as np
from object_detector import Detection

_MARGIN = 10  # pixels
_ROW_SIZE = 10  # pixels
_FONT_SIZE = 1
_FONT_THICKNESS = 1
_TEXT_COLOR = (0, 0, 255)  # red
_TEXT_COLOR_MOVING = (0, 255, 0)  # green


def visualize(
        image: np.ndarray,
        detections: List[Detection],
        multiplier
) -> np.ndarray:
    """Draws bounding boxes on the input image and return it.

  Args:
    image: The input RGB image.
    detections: The list of all "Detection" entities to be visualized.
    multiplier: Multiplier

  Returns:
    Image with bounding boxes.
  """
    for detection in detections:
        color = _TEXT_COLOR
        if detection.move:
            color = _TEXT_COLOR_MOVING

        # Draw bounding_box
        start_point = detection.bounding_box.left * multiplier, detection.bounding_box.top * multiplier
        end_point = detection.bounding_box.right * multiplier, detection.bounding_box.bottom * multiplier
        cv2.rectangle(image, start_point, end_point, color, 3)

        # Draw label and score
        category = detection.categories[0]
        class_name = category.label
        probability = round(category.score, 2)
        result_text = class_name + ' (' + str(probability) + ')'
        text_location = (_MARGIN + detection.bounding_box.left * multiplier,
                         _MARGIN + _ROW_SIZE + detection.bounding_box.top * multiplier)
        cv2.putText(image, result_text, text_location, cv2.FONT_HERSHEY_PLAIN,
                    _FONT_SIZE, color, _FONT_THICKNESS)

    return image
