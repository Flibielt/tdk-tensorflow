# Copyright 2021 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Main script to run the object detection routine."""
import math
import sys
import time

import cv2
import numpy as np
import skvideo
import skvideo.motion

import utils
from detection_logger import log_detection
from motion_detection import MotionDetection
from object_detector import Detection
from object_detector import ObjectDetector
from object_detector import ObjectDetectorOptions


# skvideo.setFFmpegPath("C:/ffmpeg/bin")

def ms():
    return round(time.time() * 1000)


MOTION_AVG_TRESHOLD = 0.3
# TF_AVG_THRESHOLD = 5
TF_AVG_THRESHOLD = 0

# Motion detection
mdetector = MotionDetection()
empty_frame = np.ndarray(shape=(2, 2), dtype=float, order='F')
# original_frames = [empty_frame, empty_frame, empty_frame]
gray_frames = [empty_frame, empty_frame, empty_frame]


def read_video_stream(name):
    frames = list()
    colored_frames = list()
    cap = cv2.VideoCapture(name)
    while (cap.isOpened()):
        ret, frame = cap.read()
        if ret == False:
            break
        colored_frames.append(frame)
        frames.append(np.array(cv2.cvtColor(
            frame, cv2.COLOR_RGB2GRAY), dtype='uint8'))
    cap.release()
    return cap


def run(model: str, camera_id: int, width: int, height: int, num_threads: int,
        enable_edge_tpu: bool, log_detections: bool) -> None:
    """Continuously run inference on images acquired from the camera.

  Args:
    model: Name of the TFLite object detection model.
    camera_id: The camera id to be passed to OpenCV.
    width: The width of the frame captured from the camera.
    height: The height of the frame captured from the camera.
    num_threads: The number of CPU threads to run the model.
    enable_edge_tpu: True/False whether the model is a EdgeTPU model.
    log_detections: True/False whether to log detections.
  """

    # Variables to calculate FPS
    counter, fps = 0, 0
    start_time = time.time()

    # Start capturing video input from the camera
    # cap = cv2.VideoCapture(camera_id)
    cap = cv2.VideoCapture('pexels-854204.mp4')
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)

    # Visualization parameters
    row_size = 20  # pixels
    left_margin = 24  # pixels
    text_color = (0, 0, 255)  # red
    font_size = 1
    font_thickness = 1
    fps_avg_frame_count = 10

    # Initialize the object detection model
    options = ObjectDetectorOptions(
        num_threads=num_threads,
        score_threshold=0.3,
        max_results=3,
        enable_edgetpu=enable_edge_tpu)
    detector = ObjectDetector(model_path=model, options=options)

    # Continuously capture images from the camera and run inference
    motion_map = np.array([])

    time_tf = 0
    time_bm = 0
    while cap.isOpened():
        success, image = cap.read()
        if success:
            nX, nY = math.floor(image.shape[1] / 4), math.floor(image.shape[0] / 4)
            if not success:
                sys.exit(
                    'ERROR: Unable to read from webcam. Please verify your webcam settings.'
                )

            counter += 1
            image = cv2.flip(image, 1)

            """Detects motion."""
            motion_detection_needed = True
            """motionDetectionNeeded = False
            if motion_map.shape[0] == 0:
                motionDetectionNeeded = True
            else:
                if counter < 4:
                    motionDetectionNeeded = True
                else:
                    prev_frame = ((gray_frames[1] + gray_frames[0]) / 2)
                    prev_frame_64 = cv2.resize(prev_frame, dsize=(64, 64), interpolation=cv2.INTER_CUBIC)
                    curr_frame_64 = cv2.resize(gray_frames[2], dsize=(64, 64), interpolation=cv2.INTER_CUBIC)
                    diffs = prev_frame_64-curr_frame_64
                    diff = diffs[diffs > 5].shape[0]
                    diffStd = abs(np.std(prev_frame)-np.std(gray_frames[2]))
                    print(f"diff {diff}, diffStd {diffStd}")

                    if diff > 100:
                        motionDetectionNeeded = True"""

            if motion_detection_needed:
                print(f"order_motion_frames() {counter}")
                order_motion_frames()
            gray_frames[2] = np.array(cv2.cvtColor(image, cv2.COLOR_RGB2GRAY), dtype='uint8')

            if motion_detection_needed:
                if counter >= 4:
                    print(f"frames {counter - 1}-{counter}")
                    prev_frame = ((gray_frames[1] + gray_frames[0]) / 2)

                    """prev_frame_128 = cv2.resize(prev_frame, dsize=(nX, nY), interpolation=cv2.INTER_CUBIC)
                    curr_frame_128 = cv2.resize(gray_frames[2], dsize=(nX, nY), interpolation=cv2.INTER_CUBIC)
                    motion_map = mdetector.diamond_search_motion_estimation(prev_frame_128, curr_frame_128)"""
                    t1 = ms()

                    channels = 1
                    if len(prev_frame.shape) > 2:
                        channels = prev_frame.shape[2]

                    videodata = np.empty((2, prev_frame.shape[0], prev_frame.shape[1], channels),
                                         dtype=prev_frame.dtype)
                    videodata[0, :, :, :] = prev_frame.reshape((prev_frame.shape[0], prev_frame.shape[1], channels))
                    videodata[1, :, :, :] = gray_frames[2].reshape((prev_frame.shape[0], prev_frame.shape[1], channels))

                    # motion_map = mdetector.diamond_search_motion_estimation(prev_frame, gray_frames[2])
                    motion_map = skvideo.motion.blockMotion(videodata, method="ARPS")
                    time_bm = time_bm + ms() - t1

            # Run object detection estimation using the model.
            npavg = motion_map[motion_map > 0.3].shape[0]
            print(f"npavg: {npavg}")
            if counter == 1 or (motion_detection_needed and npavg > TF_AVG_THRESHOLD):
                """image_128 = cv2.resize(image, (nX, nY))
                detections = detector.detect(image_128)"""
                t2 = ms()
                detections = detector.detect(image)
                time_tf = time_tf + ms() - t2
                print(f"time_tf: {time_tf}")

            # Calculate the FPS
            if counter % fps_avg_frame_count == 0:
                end_time = time.time()
                fps = fps_avg_frame_count / (end_time - start_time)
                start_time = time.time()

            # Motion detection II
            detect_motion(motion_map, counter, detections)

            if log_detections:
                log_detection(detections)

            # Draw keypoints and edges on input image
            image_with_boxes = utils.visualize(image, detections, 1)

            # Show the FPS
            fps_text = 'FPS = {:.1f}'.format(fps)
            text_location = (left_margin, row_size)
            cv2.putText(image_with_boxes, fps_text, text_location, cv2.FONT_HERSHEY_PLAIN,
                        font_size, text_color, font_thickness)

            # Stop the program if the ESC key is pressed.
            if cv2.waitKey(1) == 27:
                break
            cv2.imshow('object_detector', image_with_boxes)
        else:
            break

    cap.release()
    t2 = ms()
    print(f"run took: {time_tf}|{time_bm}")
    cv2.destroyAllWindows()


def order_motion_frames():
    """Orders motion frames. Only store the 3 needed frame."""
    for i in range(1, 3, 1):
        # original_frames[i - 1] = original_frames[i]
        gray_frames[i - 1] = gray_frames[i]


def detect_motion(motion_map, frame_count, detections):
    for idx in range(len(detections)):
        detection = detections[idx]

        if motion_map.shape[0] > 0:
            print(detection.bounding_box)
            motion_sub = motion_map[0,
                         math.floor(detection.bounding_box.top / 8):math.floor(detection.bounding_box.bottom / 8),
                         math.floor(detection.bounding_box.left / 8):math.floor(detection.bounding_box.right / 8), 0]
            motion_sub_1 = motion_map[0,
                           math.floor(detection.bounding_box.top / 8):math.floor(detection.bounding_box.bottom / 8),
                           math.floor(detection.bounding_box.left / 8):math.floor(detection.bounding_box.right / 8), 1]
            avg = motion_sub[motion_sub != 0]
            avg_1 = motion_sub_1[motion_sub_1 != 0]
            print("\tname: ", detection.categories[0].label, "avg: ", avg.shape[0], "avg_1: ", avg_1.shape[0],
                  f"motion_sub{motion_sub.shape}, motion_sub_1{motion_sub_1.shape}")

            if avg.shape[0] > 10:
                detections[idx] = Detection(detection.bounding_box, detection.categories, move=True)

    # plt.subplot(2, 2, 1), plt.imshow(original_frames[1])
    # plt.title(f'Original {frame_count}')
    # plt.subplot(2, 2, 2), plt.imshow(original_frames[2])
    # plt.title(f'Original {frame_count}')
    # plt.subplot(2, 2, (3, 4)), plt.imshow(motion_map, 'gray')
    # plt.title(f'Motion map {frame_count}-{frame_count}')
    # if not os.path.exists('./plots/'):
    #    os.makedirs('./plots/')
    # plt.savefig('./plots/frames' + str(frame_count) + '-' + str(frame_count) + '.png')
