import datetime

LOG_FILE = "detection.log"


def log_detection(detections):
    if len(detections) < 1:
        return

    with open(LOG_FILE, "a") as f:
        f.write("##NEW-DETECTION##" + str(datetime.datetime.now()) + '\n')
        for detection in detections:
            f.write(format_detection(detection))


def format_detection(detection):
    return "{0}|{1}|{2}|({3},{4}),({5},{6})\n".format(
        detection.categories[0].label, str(round(detection.categories[0].score, 2)), detection.move,
        detection.bounding_box.left, detection.bounding_box.top, detection.bounding_box.right,
        detection.bounding_box.bottom)
